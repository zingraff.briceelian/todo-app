<%@ include file="common/header.jspf"%>
<%@ include file="common/navigation.jspf"%>

<div class="container">

 <br>
 <div class="panel panel-primary">
  <div class="panel-heading">
   <h3>List of TODO's</h3>
  </div>
  <div class="panel-body">
   <table class="table table-striped">
    <thead>
     <tr>
      <th width="85%">Title</th>
      <th width="15%">Status</th>
     </tr>
    </thead>
    <tbody>
     <c:forEach items="${todos}" var="todo">
      <tr>
       <td>${todo.title}</td>
       <td>${todo.done}</td>
<!--        <td><a type="button" class="btn btn-success" -->
<%--         href="/update-todo?id=${todo.id}">Update</a> --%>
<!--        <a type="button" class="btn btn-warning" -->
<%--         href="/delete-todo?id=${todo.id}">Delete</a></td> --%>
      </tr>
     </c:forEach>
    </tbody>
   </table>
  </div>
 </div>

</div>
