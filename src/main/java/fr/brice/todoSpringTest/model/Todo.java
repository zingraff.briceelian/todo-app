package fr.brice.todoSpringTest.model;


public class Todo {
	
	
	

	 private long id;
	 public static long idCount = 0;
	 
	 private String userName;
	 
	 private String title;
	 
	
	 private Enum done;

	public Todo() {
		 
	 }

	public Todo( String userName, String title, Enum isDone) {
		super();
		this.userName = userName;
		this.title = title;
		this.done = isDone;
		this.id= idCount++;
		

	}



	public Enum getDone() {
		return done;
	}

	public void setDone(Enum done) {
		this.done = done;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}







	
	
	 
	 
	 
	 

}

