package fr.brice.todoSpringTest.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import fr.brice.todoSpringTest.model.Todo;
@Repository
public interface TodoRepository {
	List<Todo> findByUserName(String user);

}
