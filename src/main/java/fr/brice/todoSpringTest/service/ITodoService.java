package fr.brice.todoSpringTest.service;

import java.util.List;
import fr.brice.todoSpringTest.model.Todo;

public interface ITodoService {
	
	List < Todo > getTodosByUser(String name);


}
