package fr.brice.todoSpringTest.service;

import java.util.ArrayList;
import java.util.List;
import org.springframework.stereotype.Service;

import fr.brice.todoSpringTest.model.Done;
import fr.brice.todoSpringTest.model.Todo;

@Service
public class TodoService implements ITodoService {




    @Override
    public List < Todo > getTodosByUser(String name) {
    	System.out.println("ici");
    	
    	return todosData(name); 
    	
    }
    

    public static List<Todo> todosData(String userName){
    	
	List<Todo> myTodos = new ArrayList<>();
	
    	
    	Todo todo1 = new Todo(userName, "UseCase1", Done.Done);
    	Todo todo2 = new Todo(userName, "UseCase2", Done.InProgress);
    	Todo todo3 = new Todo(userName, "UseCase3", Done.InProgress);
    	Todo todo4 = new Todo(userName, "UseCase4", Done.InProgress);
    	
    	myTodos.add(todo1);
    	myTodos.add(todo2);
    	myTodos.add(todo3);
    	myTodos.add(todo4);
    	System.out.println(myTodos.get(3).getId());
    	
    	return myTodos;
    	
    }


}
