package fr.brice.todoSpringTest.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import fr.brice.todoSpringTest.service.ITodoService;
import fr.brice.todoSpringTest.service.TodoService;

@Controller
public class TodoController {

	private ITodoService todoService;
	

    @RequestMapping(value = "/list-todos", method = RequestMethod.GET)
    public String showTodos(ModelMap model) {
    	todoService = new TodoService(); 
        model.put("todos", todoService.getTodosByUser("admin"));
        return "list-todos";
    }





}
